"use strict"

/*
Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
Document Object Model (DOM) - це представлення структури HTML- або XML-документа у вигляді дерева об'єктів, яке може бути використане для динамічного змінення вмісту та структури документа за допомогою скриптових мов, таких як JavaScript.
Основна ідея DOM полягає в тому, що кожен елемент HTML-документа, такий як тег, атрибут, текстовий вузол і т. д., представлений як об'єкт в програмному інтерфейсі. Ці об'єкти утворюють ієрархічну структуру, де батьківські та дочірні елементи мають взаємозв'язки.
За допомогою DOM можна звертатися до елементів, змінювати їхні властивості, додавати нові елементи, видаляти існуючі та взагалі маніпулювати вмістом документа. DOM надає зручний інтерфейс для взаємодії з HTML-документами у веб-розробці, роблячи можливим динамічне оновлення вмісту сторінки без перезавантаження сторінки.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
innerHTML та innerText - це дві різні властивості, які дозволяють отримувати або встановлювати текстовий вміст HTML-елементів за допомогою JavaScript. Однак вони мають певні відмінності:

1) innerHTML:
innerHTML дозволяє отримувати або встановлювати HTML-структуру вмісту елемента, включаючи HTML-теги та їх вміст.
Використання innerHTML може бути потенційно небезпечним, особливо якщо дані введені користувачем не фільтруються або екрануються, оскільки воно відкриває можливість для атак XSS (Cross-Site Scripting).
Приклад використання innerHTML:
let element = document.getElementById('example');
let content = element.innerHTML;
element.innerHTML = '<p>Новий вміст</p>';

2) innerText:
innerText дозволяє отримувати або встановлювати тільки текстовий вміст елемента, ігноруючи HTML-теги.
Використання innerText є безпечнішим з точки зору безпеки, оскільки воно не враховує HTML-структуру та виключає можливість введення коду, як в разі innerHTML.
Приклад використання innerText:
let element = document.getElementById('example');
let textContent = element.innerText;
element.innerText = 'Новий текст';
Отже, основна різниця полягає в тому, що innerHTML опрацьовує HTML-структуру, включаючи теги, тоді як innerText працює тільки з текстовим вмістом і ігнорує HTML-теги.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
Є кілька способів звертання до елемента сторінки за допомогою JavaScript. Найбільш розповсюджені методи - це використання getElementById, getElementsByClassName, getElementsByTagName, а також querySelector та querySelectorAll. Кожен з цих методів має свої особливості.
1) getElementById:
Знаходить елемент за його унікальним ідентифікатором (id).
Найшвидший спосіб, оскільки id повинен бути унікальним на сторінці.
let element = document.getElementById('exampleId');

2) getElementsByClassName:
Знаходить елементи за їхнім класом.
Повертає колекцію елементів з вказаним класом.
let elements = document.getElementsByClassName('exampleClass');

3) getElementsByTagName:
Знаходить елементи за їхнім ім'ям тега.
Повертає колекцію елементів з вказаним тегом.
let elements = document.getElementsByTagName('div');

4) querySelector:
Знаходить перший елемент, який відповідає переданому селектору CSS.
Повертає тільки один елемент.
let element = document.querySelector('#exampleId');

5) querySelectorAll:
Знаходить всі елементи, які відповідають переданому селектору CSS.
Повертає колекцію елементів.
let elements = document.querySelectorAll('.exampleClass');

Вибір між цими методами залежить від ваших конкретних потреб. Якщо є унікальний id, використання getElementById є швидшим і зручнішим. 
Якщо потрібно вибрати групу елементів за класом чи тегом, використання getElementsByClassName, getElementsByTagName, querySelector, або querySelectorAll буде більш відповідним.

4. Яка різниця між nodeList та HTMLCollection?
Важливо відзначити, що в більшості сучасних браузерах NodeList та HTMLCollection роблять більшість операцій подібним чином, і їхні різниці стають менш помітними. Але деякі різниці все ще існують:

1)Тип вузлів:
NodeList може включати різні типи вузлів, такі як елементи, текстові вузли, коментарі і т.д. Він ширше визначений і може включати будь-які вузли у DOM.
HTMLCollection призначений для елементів HTML-документа і включає тільки елементи. Він є більш специфічним і зазвичай використовується тільки для колекцій елементів.
2) Живучість (Live / Static):
NodeList є живучим, що означає, що він може оновлюватися автоматично під час змін в документі. Якщо, наприклад, ви викликаєте document.querySelectorAll() і отримуєте NodeList, він буде автоматично оновлюватися, якщо DOM зміниться.
HTMLCollection може бути живучим або статичним в залежності від методу його отримання. Якщо ви, наприклад, викликаєте document.getElementsByClassName(), ви отримаєте живучий HTMLCollection, який буде оновлюватися, якщо DOM зміниться. Але якщо ви, наприклад, отримуєте children елемента, ви отримаєте статичний HTMLCollection, який не оновлюється автоматично.
3) Методи:
NodeList має деякі спеціальні методи, такі як forEach і item, а також властивість length.
HTMLCollection може мати специфічні для HTML-колекцій методи, такі як namedItem (який дозволяє звертатися до елементів за їхнім іменем, якщо вони мають id або name) і також має властивість length.
У сучасному програмуванні обидва типи дуже подібні, і ви часто можете використовувати їх взаємозамінно в залежності від конкретних обставин. Як правило, NodeList частіше використовується, оскільки він загальніший і включає в себе різні типи вузлів, але вам може бути зручно використовувати HTMLCollection, коли вам потрібно працювати тільки з елементами.


Практичні завдання
1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
Використайте 2 способи для пошуку елементів.
Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
*/

const myFeatureElems = document.getElementsByClassName('feature');
console.log("Method 1 (getElementsByClassName()): ", myFeatureElems);

for (let i = 0; i < myFeatureElems.length; i++) {
    myFeatureElems[i].style.textAlign = 'center';
}

const myFeatureSelector = document.querySelectorAll('.feature');
console.log("Method 2 (querySelectorAll()): ", myFeatureSelector);

myFeatureSelector.forEach(element => {element.style.textAlign = 'center';});

/*
2. Змініть текст усіх елементів h2 на "Awesome feature".
*/

const myHeaderElems = document.querySelectorAll('h2');
myHeaderElems.forEach(element => {element.textContent = 'Awesome feature';});

/*
3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
*/

const myFeatureTitleElems = document.querySelectorAll('.feature-title');
myFeatureTitleElems.forEach(element => {element.textContent += '!';});
